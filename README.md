## Overview

A child theme of the TwentyNineteen WordPress theme, with the addition of Bootstrap 4.0 and FontAwesome 4.7 support.

![picture](twentynineteen-child/screenshot.png)

## How to build the zip

Run the pipeline and get it from the [downloads](https://bitbucket.org/borgogelli/hello-wp-theme/downloads) section, or alternatively

```
git clone https://borgogelli@bitbucket.org/borgogelli/hello-wp-theme.git
npm install
npm run dist
# you will find the hello-wp-theme.zip archive in the dist folder
```
  
## Download

Get the last version from the [Downloads section](https://bitbucket.org/borgogelli/hello-wp-theme/downloads)


